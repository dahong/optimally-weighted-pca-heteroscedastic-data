# Optimally Weighted PCA for High-Dimensional Heteroscedastic Data

Repository containing code for reproducing the results in the paper
> David Hong, Fan Yang, Jeffrey A. Fessler, Laura Balzano. "Optimally Weighted PCA for High-Dimensional Heteroscedastic Data", 2022. https://arxiv.org/abs/1810.12862.

This repo contains a folder for reproducing each figure from the paper.

## `Figure-2/`

### Figure 2a: `d = 100, ntrials = 10000`

![Numerical simulation: d = 100, ntrials = 10000](Figure-2/outs/fig,c-4-8,v-1-3,lambda-1,d-100,ntrials-10000,ratiores-0.01.png)

### Figure 2b: `d = 1000, ntrials = 500`

![Numerical simulation: d = 1000, ntrials = 500](Figure-2/outs/fig,c-4-8,v-1-3,lambda-1,d-1000,ntrials-500,ratiores-0.01.png)

## `Figure-3/`

### Figure 3a: `λ₁ = 1`

![Weight comparison: λ₁ = 1](Figure-3/outs/fig,c-4-8,v-1-3,lambda-1,d-1000,ntrials-500,ratiores-0.01.png)

### Figure 3b: `λ₁ = 30`

![Weight comparison: λ₁ = 30](Figure-3/outs/fig,c-4-8,v-1-3,lambda-30,d-1000,ntrials-500,ratiores-0.01.png)

## `Figure-4/`

![Performance comparison](Figure-4/outs/fig,c-1-10,v1-1.0,v2range-1.0-20.0,lambda-2,d-1000,ntrials-400.png)

## `Figure-5/`

### Figure 5a: with respect to `c` and `λᵢ`

![Phase transition comparison: with respect to c and λᵢ](Figure-5/outs/fig,clam,r21-10,v-1-5.png)

### Figure 5b: with respect to `λᵢ/v₁` and `λᵢ/v₂`

![Phase transition comparison: with respect to λᵢ/v₁ and λᵢ/v₂](Figure-5/outs/fig,clam,r21-10,v-1-5.png)

## `Figure-6/`

![Performance comparison with additional methods](Figure-6/outs/fig,c-1-10,v1-1.0,v2range-1.0-20.0,lambda-2,d-1000,ntrials-400.png)

## `Figure-7/`

![Estimated variances](Figure-7/outs/fig,c-4-8,v-1-3,lambda-1,d-1000,ntrials-500,ratiores-0.01.png)

## `Figure-8-9/`

### Figure 8: "ground truth" components

![SDSS: "ground truth" components](Figure-8-9/outs/sdss,U.png)

### Figure 9a: data matrix `Y` and variance vector `v`

![SDSS: data matrix Y and variance vector v](Figure-8-9/outs/sdss,data.png)

### Figure 9b: example spectra

![SDSS: example spectra](Figure-8-9/outs/sdss,specs.png)
