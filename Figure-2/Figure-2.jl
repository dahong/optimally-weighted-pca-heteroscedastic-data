### A Pluto.jl notebook ###
# v0.17.4

using Markdown
using InteractiveUtils

# ╔═╡ edee70e9-14b9-4702-b2ed-f8343a9e1de4
import Pkg; Pkg.activate(@__DIR__)

# ╔═╡ 9d30147c-3e7d-4308-ad0f-004425cf7428
using CacheVariables, DelimitedFiles, CairoMakie, LinearAlgebra, ProgressLogging, Roots, StableRNGs, StatsBase, ThreadsX

# ╔═╡ c020ed90-6c3c-11ec-37fa-0707e9f453c2
using Logging, TerminalLoggers; global_logger(TerminalLogger());

# ╔═╡ c020ecaa-6c3c-11ec-1723-3bb53e84b37c
md"""
# Figure 2
"""

# ╔═╡ 1a8bf576-117c-422e-9ba8-77877177b663
md"""
## Setup
"""

# ╔═╡ bb7a7ce4-37f5-4663-acb3-49d778601824
c = (4,8)

# ╔═╡ 827a9cd3-0cee-465e-8125-9468a5d820cd
v = (1,3)

# ╔═╡ 614ddf89-5fef-4a62-b408-5843f7a2f15f
λ1 = 1

# ╔═╡ 24d56e8b-c202-4a99-8b74-dc34881bab5b
# d, ntrials = 100, 10000
d, ntrials = 1000, 500

# ╔═╡ 9049f14c-8c22-4b05-8205-f9e998e86726
ratiores = 0.01

# ╔═╡ 27e54a11-5d5c-4964-ba63-f6257b5d0fab
md"""
**Derived quantities:**
"""

# ╔═╡ 0a3502d9-fdc9-4243-b439-a6c8f64fb6fa
n = c.*d

# ╔═╡ 1bea3ae5-885f-419d-be1a-8bab775c2981
k = 1

# ╔═╡ 936573d0-47cd-49ad-82dd-f564ccfb7667
L = length.((c,v)) |> only∘unique

# ╔═╡ 64831d47-9930-438b-be9f-006b801a564f
CONFIGSTR = "c-$(join(c,'-')),v-$(join(v,'-')),lambda-$λ1,d-$d,ntrials-$ntrials,ratiores-$ratiores"

# ╔═╡ 001f81ca-bdef-464c-8de1-97b3859144c0
md"""
## Simulation
"""

# ╔═╡ ada674d8-a26d-41f2-aed2-0db3954fbffe
opts = cache(joinpath("cache","sim,$CONFIGSTR.bson")) do
	ratios = 0.0:ratiores:1.0
	wlist = vcat(
		[(r,1.0) for r in ratios],
		[(1.0,r) for r in ratios],
	)
	
	@withprogress map(1:ntrials) do trial
		rng = StableRNG(trial)

		# Generate data
		u1 = normalize(randn(rng,d))
		F = [sqrt(λ1)*u1;;]
		Y = [F*randn(rng,k,n[l]) + sqrt(v[l])*randn(rng,d,n[l]) for l in 1:L]

		# Compress data (to speed up sim)
		Y = map(1:L) do l
			U,s,_ = svd!(Y[l])
			U*Diagonal(s)
		end

		# Sweep through weights
		sweep = ThreadsX.map(wlist) do w
			Yw = reduce(hcat,sqrt(w[l])*Y[l] for l in 1:L)
			uh1 = svd(Yw).U[:,1]
			return abs2(u1'uh1)
		end
		
		@logprogress trial/ntrials
		return (w=wlist[argmax(sweep)],r=maximum(sweep))
	end
end

# ╔═╡ 1bd0dbe6-05b6-452a-9f46-c08bc2dfa414
w21opts = getindex.(opts,:w) .|> w->w[2]/w[1]

# ╔═╡ 30426eaf-e3a9-4a08-987c-c958b2078886
ropts = getindex.(opts,:r)

# ╔═╡ a6a57eb0-a6ea-4e6d-9698-e328e9447f61
md"""
## Asymptotics
"""

# ╔═╡ 2daacb0a-c6be-41b2-9f44-2ce0efd05b0b
w21optlim = inv.(v.*(λ1.+v)) |> w->w[2]/w[1]

# ╔═╡ 2a67e5d2-2157-4f90-af72-698fa918adc1
roptlim = let
	R = x -> 1 - λ1*sum(c[l]/v[l]*(1-x)/(v[l]/λ1+x) for l in 1:L)
	R(0.0) >= 0.0 ? 0.0 : find_zero(R,(0,1),Bisection())
end

# ╔═╡ ef8ef4ed-8ca8-4bbf-ae15-d13c020a37bc
md"""
## Plots
"""

# ╔═╡ 7a3db3a0-1266-4be0-9246-296b39223be0
WBINS = -ratiores/2:ratiores:1+ratiores/2

# ╔═╡ 62ee4e0a-24fb-43f8-a73d-26570babaa73
RBINS = 0:0.02:1

# ╔═╡ f78ed2cf-9514-4475-93f6-b2dedc12c41b
fig = with_theme(;Axis=(;xlabelsize=18f0)) do
	fig = Figure(;resolution=(800,300))

	axw = Axis(fig[1,1];
		xlabel = L"Optimized relative weight $w^{\star}_{1,2}(\mathbf{Y}) / w^{\star}_{1,1}(\mathbf{Y})$",
		limits = (0, 1/2, 0, 25),
		xticks = MultiplesTicks(3, 1/6, "/6"),
	)
	h = hist!(axw, w21opts; bins=WBINS, normalization=:pdf)
	v = vlines!(axw, [w21optlim]; linestyle=:dash, linewidth=4, color=:red)

	axr = Axis(fig[1,2];
		xlabel=L"Optimized performance $r^{\star}_{1}(\mathbf{Y})$",
		limits = (0, 1, 0, 25),
		xticks = MultiplesTicks(4, 1/4, "/4"),
	)
	hist!(axr, ropts; bins=RBINS, normalization=:pdf)
	vlines!(axr, [roptlim]; linestyle=:dash, linewidth=4, color=:red)

	Legend(fig[0,:], [h,v], ["Nonasymptotic empirical distribution","Limit"];
		orientation = :horizontal, tellheight = true, tellwidth = false,
		framevisible = false, padding = (0,0,0,0),
	)
	fig
end

# ╔═╡ 806d4c7e-41af-492d-929a-c7b5bf72999e
md"""
**Save figure and data to files:**
"""

# ╔═╡ 71f1e1d3-18d2-4cea-909b-824f0b7aab14
!ispath("outs") && mkpath("outs");

# ╔═╡ d6424d3c-85fc-4223-bef0-6738439e615c
save(joinpath("outs","fig,$CONFIGSTR.png"), fig)

# ╔═╡ 7f13d43a-807b-44db-adec-6b2140d52af3
let h = normalize(fit(Histogram,w21opts,WBINS); mode=:pdf)
	writedlm(
		joinpath("outs","opt,weight,sim,weights,$CONFIGSTR.dat"),
		[only(h.edges) [h.weights; 0.0]][1:findlast(!iszero,h.weights)+1,:]
	)
end

# ╔═╡ 670c0cf7-5e30-4f93-b475-b37b1c9c43af
let h = normalize(fit(Histogram,ropts,RBINS); mode=:pdf)
	writedlm(
		joinpath("outs","opt,weight,sim,rec,$CONFIGSTR.dat"),
		[only(h.edges) [h.weights; 0.0]][1:findlast(!iszero,h.weights)+1,:]
	)
end

# ╔═╡ Cell order:
# ╟─c020ecaa-6c3c-11ec-1723-3bb53e84b37c
# ╠═edee70e9-14b9-4702-b2ed-f8343a9e1de4
# ╠═9d30147c-3e7d-4308-ad0f-004425cf7428
# ╠═c020ed90-6c3c-11ec-37fa-0707e9f453c2
# ╟─1a8bf576-117c-422e-9ba8-77877177b663
# ╠═bb7a7ce4-37f5-4663-acb3-49d778601824
# ╠═827a9cd3-0cee-465e-8125-9468a5d820cd
# ╠═614ddf89-5fef-4a62-b408-5843f7a2f15f
# ╠═24d56e8b-c202-4a99-8b74-dc34881bab5b
# ╠═9049f14c-8c22-4b05-8205-f9e998e86726
# ╟─27e54a11-5d5c-4964-ba63-f6257b5d0fab
# ╠═0a3502d9-fdc9-4243-b439-a6c8f64fb6fa
# ╠═1bea3ae5-885f-419d-be1a-8bab775c2981
# ╠═936573d0-47cd-49ad-82dd-f564ccfb7667
# ╠═64831d47-9930-438b-be9f-006b801a564f
# ╟─001f81ca-bdef-464c-8de1-97b3859144c0
# ╠═ada674d8-a26d-41f2-aed2-0db3954fbffe
# ╠═1bd0dbe6-05b6-452a-9f46-c08bc2dfa414
# ╠═30426eaf-e3a9-4a08-987c-c958b2078886
# ╟─a6a57eb0-a6ea-4e6d-9698-e328e9447f61
# ╠═2daacb0a-c6be-41b2-9f44-2ce0efd05b0b
# ╠═2a67e5d2-2157-4f90-af72-698fa918adc1
# ╟─ef8ef4ed-8ca8-4bbf-ae15-d13c020a37bc
# ╠═7a3db3a0-1266-4be0-9246-296b39223be0
# ╠═62ee4e0a-24fb-43f8-a73d-26570babaa73
# ╠═f78ed2cf-9514-4475-93f6-b2dedc12c41b
# ╟─806d4c7e-41af-492d-929a-c7b5bf72999e
# ╠═71f1e1d3-18d2-4cea-909b-824f0b7aab14
# ╠═d6424d3c-85fc-4223-bef0-6738439e615c
# ╠═7f13d43a-807b-44db-adec-6b2140d52af3
# ╠═670c0cf7-5e30-4f93-b475-b37b1c9c43af
