### A Pluto.jl notebook ###
# v0.17.4

using Markdown
using InteractiveUtils

# ╔═╡ 97f2d6f5-5bc8-4c0a-86e6-3629be0cfd6f
import Pkg; Pkg.activate(@__DIR__)

# ╔═╡ 13418d35-fcb1-48a5-9296-30e999006f4f
using CairoMakie, DelimitedFiles, LaTeXStrings, Roots

# ╔═╡ 2d7d7584-6c60-11ec-2b7e-036b0b8c04d1
md"""
# Figure 5
"""

# ╔═╡ 70bf7e9b-93ca-4480-be72-9e466a191f03
import Contour

# ╔═╡ 6dd53f99-f847-4f7a-aced-a3fdc38ba3fa
function zerolevel(f,x,y)
	lvl = Contour.contour(x,y,[f(xi,yi) for xi in x, yi in y],0.0)
	return Contour.coordinates(only(Contour.lines(lvl)))
end

# ╔═╡ 6d98a0ff-51c9-48b0-8156-fef831a7b931
md"""
## Phase transition threshold
"""

# ╔═╡ b5dca6c1-1dea-4614-8e7d-4db38183b13b
phaseoptlim(c,v,λi) = sum(cl*(λi/vl)^2 for (cl,vl) in zip(c,v)) - 1

# ╔═╡ 82f68efb-bbff-4493-b127-019c6b616b3e
function phaseinvvar(c,v,λi)
	L = length.((c,v)) |> only∘unique
	cb = sum(c)
	p = [c[l]/cb for l in 1:L]
	vb = inv(sum(p[l]/v[l] for l in 1:L))
	return cb*(λi/vb)^2 - 1
end

# ╔═╡ 6f5e3d10-b824-4758-bff3-89eb23f1d790
phaseonlyY1(c,v,λi) = c[1]*(λi/v[1])^2 - 1

# ╔═╡ 8a4031df-2bf7-4091-8195-00c8b4403fd6
phaseonlyY2(c,v,λi) = c[2]*(λi/v[2])^2 - 1

# ╔═╡ 5ea2a4b3-7904-491c-bb8b-c98e778064a8
function phaseunweight(c,v,λi)
	L = length.((c,v)) |> only∘unique
	A = x -> 1 - sum(c[l]*v[l]^2/(x-v[l])^2 for l in 1:L)
	Bi = x -> 1 - λi*sum(c[l]/(x-v[l]) for l in 1:L)
	βi = find_zero(Bi,(maximum(v),Inf),Bisection())
	return A(βi)
end

# ╔═╡ 5304e01f-866d-402c-92e0-474d83a40495
phaselist = (;
	optlim = phaseoptlim,
	invvar = phaseinvvar,
	onlyY1 = phaseonlyY1,
	onlyY2 = phaseonlyY2,
	unweight = phaseunweight,
)

# ╔═╡ 43314c28-1eb8-4c75-baf6-2aedf31ccdce
md"""
## Plots
"""

# ╔═╡ 9ac856dc-9670-446e-9e0b-eb03fbd500fa
md"""
### With respect to $c$ and $\lambda_i$
"""

# ╔═╡ f1d1da07-9cf9-444b-9204-6701d4ae59aa
CONFIG_cλ = (;
	crange = filter(!iszero,0:0.005:6),
	λrange = filter(!iszero,0:0.005:3.2),
	r21 = 10,
	v = (1,5),
)

# ╔═╡ 436c2bf8-c9fc-47db-aecb-df7e3bd92525
CONFIGSTR_cλ = "r21-$(CONFIG_cλ.r21),v-$(join(CONFIG_cλ.v,'-'))"

# ╔═╡ decfccaa-73cd-4017-9b0f-73729a4550ab
fig_cλ = with_theme(;linewidth=2,Axis=(;xlabelsize=18f0,ylabelsize=18f0)) do
	fig = Figure(;resolution=(800,300))
	ax = Axis(fig[1,1];
		xlabel = L"Combined aspect ratio $c$",
		ylabel = L"Signal component variance $\lambda_{i}$",
		limits = (0.8, 6, 0.9, 3.2),
		xticks = 1:6, yticks = 1:3,
	)
	
	labellist = (;
		optlim = LaTeXString("Asymptotic optimal"),
		invvar = LaTeXString("Inverse noise variance"),
		onlyY1 = L"Only use $\mathbf{Y}_{1}$ (cleaner block)",
		onlyY2 = L"Only use $\mathbf{Y}_{2}$ (larger block)",
		unweight = LaTeXString("Unweighted"),
	)
	for (phase,label) in zip(phaselist,labellist)
		c, λi = zerolevel(CONFIG_cλ.crange,CONFIG_cλ.λrange) do c, λi
			p = (1,CONFIG_cλ.r21)./(1+CONFIG_cλ.r21)
			phase(c.*p,CONFIG_cλ.v,λi)
		end
		lines!(ax, c, λi; label)
	end
	
	Legend(fig[1,2], ax; framevisible=false, padding=(0,0,0,0), labelsize=18f0)
	fig
end

# ╔═╡ 68bc5b97-3365-4af2-bc34-7f739d771f00
md"""
**Save figure and data to files:**
"""

# ╔═╡ 2fbe4d36-a2aa-445d-906d-2750757077b5
!ispath("outs") && mkpath("outs");

# ╔═╡ a25600ad-c17d-44ee-b7c5-38426c46dc33
save(joinpath("outs","fig,clam,$CONFIGSTR_cλ.png"), fig_cλ)

# ╔═╡ aef4779d-fb9c-4ab2-bbb6-d66a33dc7665
for method in [:unweight]
	phase = phaselist[method]
	
	DOWNSAMPLE = 100
	c, λi = zerolevel(CONFIG_cλ.crange,CONFIG_cλ.λrange) do c, λi
		p = (1,CONFIG_cλ.r21)./(1+CONFIG_cλ.r21)
		phase(c.*p,CONFIG_cλ.v,λi)
	end
	idx = [1:DOWNSAMPLE:length(c)-1; length(c)] |> reverse
	writedlm(
		joinpath("outs","weight,comp,phase,clam,$CONFIGSTR_cλ,$method.dat"),
		["c" "lam"; round.(c[idx];digits=3) round.(λi[idx];digits=3)]
	)
end

# ╔═╡ c37402dc-1bc4-42ac-9e55-57cbc5aae2e8
md"""
### With respect to $\lambda_i/v_1$ and $\lambda_i/v_2$
"""

# ╔═╡ 624836d7-9f21-4a51-b43b-034193f4c206
CONFIG_λv12 = (;
	c = 1,
	r21 = 1,
	λv1range = filter(!iszero,0:0.002:2.2),
	λv2range = filter(!iszero,0:0.002:2.2),
)

# ╔═╡ dfedfe02-6d38-428b-b45c-150e3b140111
CONFIGSTR_λv12 = "c-$(CONFIG_λv12.c),r21-$(CONFIG_λv12.r21)"

# ╔═╡ d6d62b5c-013b-46f1-93d3-8a4b547cfe3e
fig_λv12 = with_theme(;linewidth=2,Axis=(;xlabelsize=18f0,ylabelsize=18f0)) do
	fig = Figure(;resolution=(800,300))
	ax = Axis(fig[1,1];
		xlabel = L"Signal-to-noise ratio $\lambda_{i}/v_{1}$",
		ylabel = L"Signal-to-noise ratio $\lambda_{i}/v_{2}$",
		limits = (0, 2.2, 0, 2.2),
		xticks = 0:0.5:2, yticks = 0:0.5:2,
	)
	
	labellist = (;
		optlim = LaTeXString("Asymptotic optimal"),
		invvar = LaTeXString("Inverse noise variance"),
		onlyY1 = L"Only use $\mathbf{Y}_{1}$",
		onlyY2 = L"Only use $\mathbf{Y}_{2}$",
		unweight = LaTeXString("Unweighted"),
	)
	for (phase,label) in zip(phaselist,labellist)
		λv1, λv2 = zerolevel(CONFIG_λv12.λv1range,CONFIG_λv12.λv2range) do λv1, λv2
			p = (1,CONFIG_λv12.r21)./(1+CONFIG_λv12.r21)
			phase(CONFIG_λv12.c.*p,inv.((λv1,λv2)),1.0)
		end
		lines!(ax, λv1, λv2; label)
	end

	Legend(fig[1,2], ax; framevisible=false, padding=(0,0,0,0), labelsize=18f0)
	colsize!(fig.layout, 1, Aspect(1, 1))
	fig
end

# ╔═╡ 6cc731f8-e619-4702-ad6d-786333fb6655
md"""
**Save figure and data to files:**
"""

# ╔═╡ 38f3a0a8-aeb1-416e-ad43-035ae9da2f04
!ispath("outs") && mkpath("outs");

# ╔═╡ dbb5598b-6472-4eb3-8ad0-c7d4a4d74c86
save(joinpath("outs","fig,snr,$CONFIGSTR_λv12.png"), fig_λv12)

# ╔═╡ c8d80b2d-3d11-4e8c-818b-62dfd649b64b
for method in [:unweight]
	phase = phaselist[method]
	
	DOWNSAMPLE = 50
	λv1, λv2 = zerolevel(CONFIG_λv12.λv1range,CONFIG_λv12.λv2range) do λv1, λv2
		p = (1,CONFIG_λv12.r21)./(1+CONFIG_λv12.r21)
		phase(CONFIG_λv12.c.*p,inv.((λv1,λv2)),1.0)
	end
	idx = [1:DOWNSAMPLE:length(λv1)-1; length(λv1)]
	writedlm(
		joinpath("outs","weight,comp,phase,snr,$CONFIGSTR_λv12,$method.dat"),
		["lv1" "lv2"; round.(λv1[idx];digits=3) round.(λv2[idx];digits=3)]
	)
end

# ╔═╡ Cell order:
# ╟─2d7d7584-6c60-11ec-2b7e-036b0b8c04d1
# ╠═97f2d6f5-5bc8-4c0a-86e6-3629be0cfd6f
# ╠═13418d35-fcb1-48a5-9296-30e999006f4f
# ╠═70bf7e9b-93ca-4480-be72-9e466a191f03
# ╠═6dd53f99-f847-4f7a-aced-a3fdc38ba3fa
# ╟─6d98a0ff-51c9-48b0-8156-fef831a7b931
# ╠═b5dca6c1-1dea-4614-8e7d-4db38183b13b
# ╠═82f68efb-bbff-4493-b127-019c6b616b3e
# ╠═6f5e3d10-b824-4758-bff3-89eb23f1d790
# ╠═8a4031df-2bf7-4091-8195-00c8b4403fd6
# ╠═5ea2a4b3-7904-491c-bb8b-c98e778064a8
# ╠═5304e01f-866d-402c-92e0-474d83a40495
# ╟─43314c28-1eb8-4c75-baf6-2aedf31ccdce
# ╟─9ac856dc-9670-446e-9e0b-eb03fbd500fa
# ╠═f1d1da07-9cf9-444b-9204-6701d4ae59aa
# ╠═436c2bf8-c9fc-47db-aecb-df7e3bd92525
# ╠═decfccaa-73cd-4017-9b0f-73729a4550ab
# ╟─68bc5b97-3365-4af2-bc34-7f739d771f00
# ╠═2fbe4d36-a2aa-445d-906d-2750757077b5
# ╠═a25600ad-c17d-44ee-b7c5-38426c46dc33
# ╠═aef4779d-fb9c-4ab2-bbb6-d66a33dc7665
# ╟─c37402dc-1bc4-42ac-9e55-57cbc5aae2e8
# ╠═624836d7-9f21-4a51-b43b-034193f4c206
# ╠═dfedfe02-6d38-428b-b45c-150e3b140111
# ╠═d6d62b5c-013b-46f1-93d3-8a4b547cfe3e
# ╟─6cc731f8-e619-4702-ad6d-786333fb6655
# ╠═38f3a0a8-aeb1-416e-ad43-035ae9da2f04
# ╠═dbb5598b-6472-4eb3-8ad0-c7d4a4d74c86
# ╠═c8d80b2d-3d11-4e8c-818b-62dfd649b64b
