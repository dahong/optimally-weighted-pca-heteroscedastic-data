### A Pluto.jl notebook ###
# v0.17.4

using Markdown
using InteractiveUtils

# ╔═╡ edee70e9-14b9-4702-b2ed-f8343a9e1de4
import Pkg; Pkg.activate(@__DIR__)

# ╔═╡ 9d30147c-3e7d-4308-ad0f-004425cf7428
using CacheVariables, CairoMakie, DelimitedFiles, LaTeXStrings, LinearAlgebra, ProgressLogging, Roots, StableRNGs, Statistics, StructArrays, ThreadsX

# ╔═╡ c020ed90-6c3c-11ec-37fa-0707e9f453c2
using Logging, TerminalLoggers; global_logger(TerminalLogger());

# ╔═╡ c020ecaa-6c3c-11ec-1723-3bb53e84b37c
md"""
# Figure 4
"""

# ╔═╡ 1a8bf576-117c-422e-9ba8-77877177b663
md"""
## Setup
"""

# ╔═╡ bb7a7ce4-37f5-4663-acb3-49d778601824
c = (1,10)

# ╔═╡ 6f37cb87-d6cf-4a19-9eef-8e02978b0b87
v1 = 1.0

# ╔═╡ 827a9cd3-0cee-465e-8125-9468a5d820cd
v2range = 1:0.5:20

# ╔═╡ 614ddf89-5fef-4a62-b408-5843f7a2f15f
λ1 = 2

# ╔═╡ 24d56e8b-c202-4a99-8b74-dc34881bab5b
d = 1000

# ╔═╡ 9febcc79-4e4e-422c-81f2-5cdf24bb22ab
ntrials = 400

# ╔═╡ 27e54a11-5d5c-4964-ba63-f6257b5d0fab
md"""
**Derived quantities:**
"""

# ╔═╡ 0a3502d9-fdc9-4243-b439-a6c8f64fb6fa
n = c.*d

# ╔═╡ 1bea3ae5-885f-419d-be1a-8bab775c2981
k = 1

# ╔═╡ 936573d0-47cd-49ad-82dd-f564ccfb7667
L = 2

# ╔═╡ d12b2c67-545d-4767-8086-b6f879de9d27
CONFIGSTR = "c-$(join(c,'-')),v1-$v1,v2range-$(join(extrema(v2range),'-')),lambda-$λ1,d-$d,ntrials-$ntrials"

# ╔═╡ 001f81ca-bdef-464c-8de1-97b3859144c0
md"""
## Simulation
"""

# ╔═╡ ada674d8-a26d-41f2-aed2-0db3954fbffe
sim = @withprogress map(enumerate(v2range)) do (v2idx,v2)
	v = (v1,v2)
	wlist = (;
		optlim = inv.(v.*(λ1.+v)),
		invvar = inv.(v),
		onlyY1 = (1,0),
		onlyY2 = (0,1),
		unweight = (1,1),
	)
	SIMSTR = "c-$(join(c,'-')),v-$(join(v,'-')),lambda-$λ1,d-$d,ntrials-$ntrials"
	
	outslist = cache(joinpath("cache","sim,$SIMSTR.bson")) do
		outs = ThreadsX.map(1:ntrials) do trial
			rng = StableRNG(trial)

			# Generate data
			u1 = normalize(randn(rng,d))
			F = [sqrt(λ1)*u1;;]
			Y = [F*randn(rng,k,n[l]) + sqrt(v[l])*randn(rng,d,n[l]) for l in 1:L]
			
			# Compress data (to speed up sim)
			Y = map(1:L) do l
				U,s,_ = svd!(Y[l])
				U*Diagonal(s)
			end
			
			# Evaluate all weights
			rlist = map(wlist) do w
				Yw = reduce(hcat,sqrt(w[l])*Y[l] for l in 1:L)
				uh1 = svd(Yw).U[:,1]
				return abs2(u1'uh1)
			end
			return rlist
		end
		outs |> StructArray |> StructArrays.components
	end
	@logprogress v2idx/length(v2range)
	return outslist
end

# ╔═╡ 1b77cee6-a85a-4125-b9cd-a5a6b3e08789
rsimlist = sim |> StructArray |> StructArrays.components

# ╔═╡ a6a57eb0-a6ea-4e6d-9698-e328e9447f61
md"""
## Asymptotic performances
"""

# ╔═╡ 17f49e08-a90e-402b-8a3c-936f4a541235
vlist = [(v1,v2) for v2 in v2range]

# ╔═╡ 8fa3fbe5-b7b5-4fdf-9529-1bd732bdac77
roptlim = map(vlist) do v
	sum(c[l]*(λ1/v[l])^2 for l in 1:L) <= 1.0 && return 0.0
	return find_zero((0,1),Bisection()) do x
		sum(c[l]/(v[l]/λ1) * (1-x)/(v[l]/λ1+x) for l in 1:L) - 1
	end
end

# ╔═╡ 4aa4f249-5d20-46dc-9214-be428f6036ba
rinvvar = map(vlist) do v
	cb = sum(c)
	p = [c[l]/cb for l in 1:L]
	vb = inv(sum(p[l]/v[l] for l in 1:L))
	return max(0.0,(cb-vb^2/λ1^2)/(cb+vb/λ1))
end

# ╔═╡ 104d43f9-da34-40a9-98b0-26bb9300c9cd
ronlyY1 = map(vlist) do v
	return max(0.0,(c[1]-v[1]^2/λ1^2)/(c[1]+v[1]/λ1))
end

# ╔═╡ a367066d-a5dc-494c-9ec0-cd8d775235e9
ronlyY2 = map(vlist) do v
	return max(0.0,(c[2]-v[2]^2/λ1^2)/(c[2]+v[2]/λ1))
end

# ╔═╡ 32d94e79-01c3-4423-ae26-1fe389664fd8
runweight = map(vlist) do v
	A = x -> 1 - sum(c[l]*v[l]^2/(x-v[l])^2 for l in 1:L)
	B1 = x -> 1 - λ1*sum(c[l]/(x-v[l]) for l in 1:L)
	B1p = x -> λ1*sum(c[l]/(x-v[l])^2 for l in 1:L)
	β1 = find_zero(B1,(maximum(v),Inf),Bisection())
	return max(0.0,A(β1)/(β1*B1p(β1)))
end

# ╔═╡ 2f8c3508-fc75-40e8-9174-731a91bc4dde
rlimlist = (;
	optlim = roptlim,
	invvar = rinvvar,
	onlyY1 = ronlyY1,
	onlyY2 = ronlyY2,
	unweight = runweight,
)

# ╔═╡ ef8ef4ed-8ca8-4bbf-ae15-d13c020a37bc
md"""
## Plots
"""

# ╔═╡ f78ed2cf-9514-4475-93f6-b2dedc12c41b
fig = with_theme(;linewidth=2,Axis=(;xlabelsize=18f0,ylabelsize=18f0)) do
	fig = Figure(;resolution=(800,300))
	ax = Axis(fig[1,1];
		xlabel = L"Block 2 noise variance $v_2$",
		ylabel = L"r_{1}(\mathbf{w},\mathbf{Y})",
		limits = (1, 20, 0, 1),
		xticks = 1:20, yticks = MultiplesTicks(4, 1/4, "/4"),
	)

	labellist = (;
		optlim = LaTeXString("Asymp. optimal"),
		invvar = LaTeXString("Inverse noise var."),
		onlyY1 = L"Only use $\mathbf{Y}_{1}$",
		onlyY2 = L"Only use $\mathbf{Y}_{2}$",
		unweight = LaTeXString("Unweighted"),
	)
	for (rsim,rlim,label) in zip(rsimlist,rlimlist,labellist)
		band!(ax, v2range, quantile.(rsim,0.25), quantile.(rsim,0.75); label)
		lines!(ax, v2range, mean.(rsim))
		lines!(ax, v2range, rlim; color=:black, linestyle=:dash)
	end

	Legend(fig[1,2], ax; framevisible=false, padding=(0,0,0,0), labelsize=18f0)
	fig
end

# ╔═╡ 806d4c7e-41af-492d-929a-c7b5bf72999e
md"""
**Save figure and data to files:**
"""

# ╔═╡ 5b411821-b73a-4507-a240-c3845f66897b
!ispath("outs") && mkpath("outs");

# ╔═╡ 1609383f-c6f8-4643-88eb-763ca3ef4d37
save(joinpath("outs","fig,$CONFIGSTR.png"), fig)

# ╔═╡ a46f99ea-6121-49ff-8ad3-c69b3bc0e7c5
for method in keys(rsimlist)
	rsim, rlim = rsimlist[method], rlimlist[method]
	writedlm(
		joinpath("outs","weight,comp,recsweep,$CONFIGSTR,$method.dat"),
		[
			"v21" "avg" "q1" "q3" "lim";
			v2range mean.(rsim) quantile.(rsim,0.25) quantile.(rsim,0.75) rlim
		]
	)
end

# ╔═╡ Cell order:
# ╟─c020ecaa-6c3c-11ec-1723-3bb53e84b37c
# ╠═edee70e9-14b9-4702-b2ed-f8343a9e1de4
# ╠═9d30147c-3e7d-4308-ad0f-004425cf7428
# ╠═c020ed90-6c3c-11ec-37fa-0707e9f453c2
# ╟─1a8bf576-117c-422e-9ba8-77877177b663
# ╠═bb7a7ce4-37f5-4663-acb3-49d778601824
# ╠═6f37cb87-d6cf-4a19-9eef-8e02978b0b87
# ╠═827a9cd3-0cee-465e-8125-9468a5d820cd
# ╠═614ddf89-5fef-4a62-b408-5843f7a2f15f
# ╠═24d56e8b-c202-4a99-8b74-dc34881bab5b
# ╠═9febcc79-4e4e-422c-81f2-5cdf24bb22ab
# ╟─27e54a11-5d5c-4964-ba63-f6257b5d0fab
# ╠═0a3502d9-fdc9-4243-b439-a6c8f64fb6fa
# ╠═1bea3ae5-885f-419d-be1a-8bab775c2981
# ╠═936573d0-47cd-49ad-82dd-f564ccfb7667
# ╠═d12b2c67-545d-4767-8086-b6f879de9d27
# ╟─001f81ca-bdef-464c-8de1-97b3859144c0
# ╠═ada674d8-a26d-41f2-aed2-0db3954fbffe
# ╠═1b77cee6-a85a-4125-b9cd-a5a6b3e08789
# ╟─a6a57eb0-a6ea-4e6d-9698-e328e9447f61
# ╠═17f49e08-a90e-402b-8a3c-936f4a541235
# ╠═8fa3fbe5-b7b5-4fdf-9529-1bd732bdac77
# ╠═4aa4f249-5d20-46dc-9214-be428f6036ba
# ╠═104d43f9-da34-40a9-98b0-26bb9300c9cd
# ╠═a367066d-a5dc-494c-9ec0-cd8d775235e9
# ╠═32d94e79-01c3-4423-ae26-1fe389664fd8
# ╠═2f8c3508-fc75-40e8-9174-731a91bc4dde
# ╟─ef8ef4ed-8ca8-4bbf-ae15-d13c020a37bc
# ╠═f78ed2cf-9514-4475-93f6-b2dedc12c41b
# ╟─806d4c7e-41af-492d-929a-c7b5bf72999e
# ╠═5b411821-b73a-4507-a240-c3845f66897b
# ╠═1609383f-c6f8-4643-88eb-763ca3ef4d37
# ╠═a46f99ea-6121-49ff-8ad3-c69b3bc0e7c5
